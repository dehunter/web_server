package com.webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;

public class WebServer {

	private ServerSocket server;
	private Socket socket;
	
	public WebServer(){
		try {
			Map<String,String> map = new XmlBuilder().parseElement("/web_server.xml");
			server = new ServerSocket(Integer.parseInt(map.get("port").toString()));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startServer(){
		System.out.println("****************WebServer start**********************");
		while(true){
			try {
				socket = server.accept();
				new Processor(socket).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void test(){
		System.out.println(System.getProperty("user.dir"));
	}
	
	public static void main(String[] args) {
		new WebServer().startServer();
//		new WebServer().test();
	}

}
