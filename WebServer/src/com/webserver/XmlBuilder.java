package com.webserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class XmlBuilder {

	public XmlBuilder(){
		
	}
	
	public HashMap<String,String> parseElement(String xmlPath) throws Exception{
		HashMap<String,String> map = new HashMap<String,String>();
		SAXReader saxReader = new SAXReader();
		Document doc = saxReader.read(this.getClass().getResourceAsStream(xmlPath));
		Element root = doc.getRootElement();
		List<Element> eList = root.elements();
		for(Element e:eList){
			if(e.elements().size() == 0){
				map.put(e.getName(), e.getText());
			}else{
				List<Element> subList = e.elements();
				for(Element sub:subList){
					map.put(e.getName() + "." + sub.getName(), sub.getText());
				}
			}
		}
        System.out.println("MVC XML:" + map);
        return map;
	}
	
	public static void main(String[] args) {
		XmlBuilder x = new XmlBuilder();
		try {
			Map map = x.parseElement("/web_server.xml");
			System.out.println(map.get("port").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
